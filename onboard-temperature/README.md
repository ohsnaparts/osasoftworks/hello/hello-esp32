# ESP32 Onboard Temperature

ESP32-S2 and ESP32-C2 both include an onboard temperature sensor.\
It's usage is demonstrated by a dedicated REST endpoint in the

> [osa/Hello-Webserver][osa_hello-esp32_hello-webserver] example

# Visuals

![shows the gif from hello-webserver demonstrating the result of the /temperature REST endpoint](../hello-webserver/images/hello-webserver.gif)

# Infos

The readings are exposed via `driver/temp_sensor.h`.\
Please see [official documentation][esp32s2_docs_temp_sensor] or [esp32s2/examples][esp32s2_examples_temp_sensor] for more information.


[esp32s2_examples_temp_sensor]: https://github.com/espressif/esp-idf/tree/451ce8a/examples/peripherals/temp_sensor
[esp32s2_docs_temp_sensor]: https://docs.espressif.com/projects/esp-idf/en/latest/esp32s2/api-reference/peripherals/temp_sensor.html
[osa_hello-esp32_hello-webserver]: ../hello-webserver
