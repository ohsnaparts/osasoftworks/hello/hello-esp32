# Controlling RGB LEDs with Pulse Width Modulation

The program switches softly between different colors

![Shows breadboard with RGB leds dimmed on and off in different colors with an oscilloscope above showing the electrical duty cycles](./images/demo.gif)


![Circuit: GND-380R-LED(10mA)-D22(3.3V)](./images/circuit.jpg)
Created with [CircuitLab](https://www.circuitlab.com/editor/)

Espressif LEDC is made for LEDs and exposes 3V so no resistors are needed in this configuration.


Through pulsating digital highs and digital lows, an analog signal is simulated. 

![5bb319241894c437dd9b9c074fdda5e0.png](images/5bb319241894c437dd9b9c074fdda5e0.png)

**x-Axis**: PWM Frequency (how long 1 cycle lasts)
**y-Axis**: Voltage output by PWM Pin
**Duty Cycle**: Determines the HIGH percentage of 1 cycle
 
$
V_{Output} = V_{Avg} \approx  V_{Input} * Duty Cycle
$

So a duty cycle of 100% with a Voltage of 5V results in an average voltage of 5.
Reducing the duty cycle to 50%, the average voltage received is 5V * 50% = 2.5V

![da42542fba3ff725c9deb9276f650ea9.png](./images/da42542fba3ff725c9deb9276f650ea9.png)

## Example ESP32

From the [espressif Docs](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/ledc.html#ledc-api-high-low-speed-mode): 

The LED PWM Controller is designed primarily to drive LEDs. It provides a wide resolution for PWM duty cycle settings. For instance, the PWM frequency of 5 kHz can have the maximum duty resolution of 13 bits. It means that the duty can be set anywhere from 0 to 100% with a resolution of ~0.012% (2 ** 13 = 8192 discrete levels of the LED intensity).


![1412d10b40487914a1b38dfefdb9b0a2.png](./images/1412d10b40487914a1b38dfefdb9b0a2.png)


