/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "driver/ledc.h"

#define LEDC_HS_TIMER LEDC_TIMER_0
#define LEDC_HS_MODE LEDC_HIGH_SPEED_MODE

#define LEDC_HS_CH0_GPIO_BLUE (18)
#define LEDC_HS_CH1_GPIO_RED (19)
#define LEDC_HS_CH2_GPIO_GREEN (17)

#define LEDC_HS_CH0_CHANNEL_BLUE LEDC_CHANNEL_0
#define LEDC_HS_CH1_CHANNEL_RED LEDC_CHANNEL_1
#define LEDC_HS_CH2_CHANNEL_GREEN LEDC_CHANNEL_2

#define LEDC_CHANNEL_NUMBER (3)

void set_color_hard(ledc_channel_config_t channel, int32_t duty)
{
    // configure channel updates
    ledc_set_duty(
        channel.speed_mode,
        channel.channel,
        duty);

    // activate changes
    ledc_update_duty(channel.speed_mode, channel.channel);
}

void set_color_soft(ledc_channel_config_t channel, int32_t duty, int32_t fade_time_ms)
{
    ledc_set_fade_with_time(channel.speed_mode, channel.channel, duty, fade_time_ms);
    ledc_fade_start(channel.speed_mode, channel.channel, LEDC_FADE_NO_WAIT);
}

void set_led_color(ledc_channel_config_t rgb_channels[], int32_t duty_red, int32_t duty_green, int32_t duty_blue, int32_t fade_time_ms)
{
    ledc_channel_config_t channel_red = rgb_channels[0];
    ledc_channel_config_t channel_green = rgb_channels[1];
    ledc_channel_config_t channel_blue = rgb_channels[2];

    if (fade_time_ms > 0)
    {
        set_color_soft(channel_red, duty_red, fade_time_ms);
        set_color_soft(channel_green, duty_green, fade_time_ms);
        set_color_soft(channel_blue, duty_blue, fade_time_ms);
    }
    else
    {
        set_color_hard(channel_red, duty_red);
        set_color_hard(channel_green, duty_green);
        set_color_hard(channel_blue, duty_blue);
    }
}

void app_main(void)
{

    /*
     * Prepare and set configuration of timers
     * that will be used by LED Controller
     */
    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_13_BIT, // 13bit = 2^13 = 8192 levels within 1 cycle
        .freq_hz = 5000,                      // 5kHz = 1 cycle lasts 1/5000s
        .speed_mode = LEDC_HS_MODE,           // high speed mode that supports fading change
        .timer_num = LEDC_HS_TIMER,           // TODO: still unsure what this timer does and how to choose it
        .clk_cfg = LEDC_AUTO_CLK              // Auto select the source clock
    };

    ledc_timer_config(&ledc_timer);

    /*
     * Prepare individual configuration
     * for each channel of LED Controller
     * by selecting:
     * - controller's channel number
     * - output duty cycle, set initially to 0
     * - GPIO number where LED is connected to
     * - speed mode, either high or low
     * - timer servicing selected channel
     *   Note: if different channels use one timer,
     *         then frequency and bit_num of these channels
     *         will be the same
     */

    // Red, Gree, Blue
    ledc_channel_config_t ledc_channel[LEDC_CHANNEL_NUMBER] = {
        {.channel = LEDC_HS_CH0_CHANNEL_BLUE,
         .duty = 0,
         .gpio_num = LEDC_HS_CH0_GPIO_BLUE,
         .speed_mode = LEDC_HS_MODE,
         .hpoint = 0,
         .timer_sel = LEDC_HS_TIMER},
        {.channel = LEDC_HS_CH1_CHANNEL_RED,
         .duty = 0,
         .gpio_num = LEDC_HS_CH1_GPIO_RED,
         .speed_mode = LEDC_HS_MODE,
         .hpoint = 0,
         .timer_sel = LEDC_HS_TIMER},
        {.channel = LEDC_HS_CH2_CHANNEL_GREEN,
         .duty = 0,
         .gpio_num = LEDC_HS_CH2_GPIO_GREEN,
         .speed_mode = LEDC_HS_MODE,
         .hpoint = 0,
         .timer_sel = LEDC_HS_TIMER}};
    ;

    ledc_channel_config_t channel_red = ledc_channel[0];
    ledc_channel_config_t channel_green = ledc_channel[1];
    ledc_channel_config_t channel_blue = ledc_channel[2];

    ledc_channel_config(&channel_red);
    ledc_channel_config(&channel_green);
    ledc_channel_config(&channel_blue);

    const int32_t min_duty = 0;
    const int32_t max_duty = 8192 - 1;
    const int32_t fade_time_ms = 2000;
    const int32_t delay = fade_time_ms;

    while (1)
    {
        //Initialize fade service.
        ledc_fade_func_install(0);

        set_led_color(
            ledc_channel,
            random() % max_duty,
            random() % max_duty,
            random() % max_duty,
            fade_time_ms);

        vTaskDelay(delay / portTICK_PERIOD_MS);
    }
}