#include "main.h"

extern "C"
{
#include "wifi_setup.h"
#include "mqtt_setup.h"
}

static const char *TAG = "toggle_led";
NumberDisplay number_display;

void handle_mqtt_mesasge(char *message);
void draw_screen(NumberDisplay *display, int number);

extern "C" void app_main()
{
    draw_screen(&number_display, 0);

    create_wifi_station();
    setup_mqtt_connection(handle_mqtt_mesasge);
}

void handle_mqtt_mesasge(char *message)
{
    ESP_LOGI(TAG, "=======================================");
    ESP_LOGI(TAG, "Received message: %s", message);
    ESP_LOGI(TAG, "=======================================");

    number_display.clearScreen();
    number_display.displayEllipses(true);

    cJSON *pack = cJSON_Parse(message);
    cJSON *segment = cJSON_GetArrayItem(pack, 0);
    cJSON *n = cJSON_GetObjectItem(segment, "n");
    if (cJSON_IsNumber(n))
    {
        draw_screen(&number_display, n->valueint);
    }

    // free memory
    cJSON_Delete(pack);
}

void draw_screen(NumberDisplay *display, int number)
{
    auto random = rand() % 100;
    auto filledEllipses = random % 2 == 1;

    display->clearScreen();
    display->displayEllipses(filledEllipses);
    display->displayNumber(number);

    // delay(2000);
}