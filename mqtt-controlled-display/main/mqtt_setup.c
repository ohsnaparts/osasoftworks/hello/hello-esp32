#include "mqtt_setup.h"

// configured using Kconfig.projbuild and idf menuconfig
#define ESP_MQTT_URI CONFIG_ESP_MQTT_BROKER_URI
#define ESP_MQTT_CONTROL_TOPIC CONFIG_ESP_MQTT_CONTROL_TOPIC
#define ESP_MQTT_DATA_TOPIC CONFIG_ESP_MQTT_DATA_TOPIC
#define ESP_MQTT_BROKER_USER CONFIG_ESP_MQTT_BROKER_USER
#define ESP_MQTT_BROKER_PASSWORD CONFIG_ESP_MQTT_BROKER_PASSWORD
#define LEDC_TRANSITION_INTERVAL CONFIG_PULSE_WIDTH_MODULATION_FADE_INTERVAL

static const char *TAG = "zube.mqtt_setup";
void (*mqtt_message_handler)(char *);

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    handle_mqtt_event(event_data);
}

void setup_mqtt_connection(void (*message_handler)(char *))
{
    mqtt_message_handler = message_handler;
    esp_mqtt_client_config_t mqtt_configuration = {
        .uri = ESP_MQTT_URI,
        .username = ESP_MQTT_BROKER_USER,
        .password = ESP_MQTT_BROKER_PASSWORD};

    ESP_LOGV(TAG, "Initializing MQTT client...");
    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_configuration);

    ESP_LOGV(TAG, "Wiring up MQTT callbacks...");
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);

    ESP_LOGV(TAG, "Starting MQTT client...");
    esp_mqtt_client_start(client);
}

// TODO: should event be a pointer?
void handle_mqtt_event(esp_mqtt_event_handle_t event)
{
    switch (event->event_id)
    {
    case MQTT_EVENT_CONNECTED:
        mqtt_connected_handler(event);
        break;
    case MQTT_EVENT_DISCONNECTED:
        mqtt_disconnected_handler(event);
        break;
    case MQTT_EVENT_SUBSCRIBED:
        mqtt_subscribed_handler(event);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        mqtt_unsubscribed_handler(event);
        break;
    case MQTT_EVENT_PUBLISHED:
        mqtt_published_handler(event);
        break;
    case MQTT_EVENT_DATA:
        mqtt_data_handler(event);
        break;
    case MQTT_EVENT_ERROR:
        mqtt_error_handler(event);
        break;
    case MQTT_EVENT_BEFORE_CONNECT:
        mqtt_before_connect_handler(event);
        break;
    default:
        mqtt_unknown_handler(event);
        break;
    }
}

void mqtt_connected_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGD(TAG, "MQTT_EVENT_CONNECTED");

    esp_mqtt_client_handle_t client = event->client;
    esp_mqtt_client_publish(client, ESP_MQTT_DATA_TOPIC, "MQTT_EVENT_CONNECTED", 0, 1, 0);
    esp_mqtt_client_subscribe(client, ESP_MQTT_CONTROL_TOPIC, 0);
}

void mqtt_disconnected_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGD(TAG, "MQTT_EVENT_DISCONNECTED");
    esp_mqtt_client_handle_t client = event->client;
    esp_mqtt_client_publish(client, ESP_MQTT_DATA_TOPIC, "MQTT_EVENT_DISCONNECTED", 0, 1, 0);
}

void mqtt_subscribed_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGD(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
    esp_mqtt_client_handle_t client = event->client;
    esp_mqtt_client_publish(client, ESP_MQTT_DATA_TOPIC, "MQTT_EVENT_SUBSCRIBED", 0, 1, 0);
}

void mqtt_unsubscribed_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
    esp_mqtt_client_publish(client, ESP_MQTT_DATA_TOPIC, "MQTT_EVENT_UNSUBSCRIBED", 0, 1, 0);
}

void mqtt_published_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
}

void mqtt_error_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
}

void mqtt_any_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGI(TAG, "MQTT_EVENT_ANY");
}

void mqtt_before_connect_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGI(TAG, "MQTT_EVENT_BEFORE_CONNECT");
}

void mqtt_unknown_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGI(TAG, "Unknown event type received");
    esp_mqtt_client_handle_t client = event->client;
    esp_mqtt_client_publish(client, ESP_MQTT_DATA_TOPIC, "OTHER EVENT", 0, 1, 0);
}

void mqtt_data_handler(esp_mqtt_event_handle_t event)
{
    ESP_LOGI(TAG, "MQTT_EVENT_DATA");
    char *data = event->data;
    ESP_LOGD(TAG, "TOPIC=%.*s\n", event->topic_len, event->topic);
    ESP_LOGD(TAG, "DATA=%.*s\n", event->data_len, data);

    mqtt_message_handler(data);
}