/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "mcuinfo.h"

#define RESTART_COUNTDOWN_SECONDS 5
#define HELLO_NOTIFICATION_COUNT 5
#define HELLO_NOTIFICATION_INTERVAL_MS 500


extern "C" {
    void app_main();
}

void sleep_ms(int milliseconds) {
    vTaskDelay(milliseconds / portTICK_PERIOD_MS);
}

void app_main(void)
{
    esp_mcuinfo mcuinfo;
    for (int i = HELLO_NOTIFICATION_COUNT; i >= 0; i--)
    {
        mcuinfo.print_info();
        sleep_ms(HELLO_NOTIFICATION_INTERVAL_MS);
    }


    printf("Restarting now.\n");
    for (int i = RESTART_COUNTDOWN_SECONDS; i >= 0; i--)
    {
        printf("Restarting in %d seconds...\n", i);
        sleep_ms(1000);
    }

    fflush(stdout);
    esp_restart();
}
