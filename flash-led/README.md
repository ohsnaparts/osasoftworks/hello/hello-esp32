# Flash LED

Starts a FreeRTOS task to blink a LED Diode.

![Circuit: GND-380R-LED(10mA)-D22(3.3V)](./images/circuit.jpg)
Created with [CircuitLab](https://www.circuitlab.com/editor/)

```
I = 10mA = 0.01A
U = 3.3V
R = U / I 
  = 3.3/0.01 = 330Ohm
  = The resistor has to have at least 330Ohms of resistance
```

The program turns the LED on and off in a rhythmical manner

![Shows breadboard curcuit with red led flashing](./images/led-demo.gif)
