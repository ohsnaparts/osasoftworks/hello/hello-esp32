#include "esp_log.h"
#include "font6x9.h"
#include "demos.h"
#include "hagl_string_extensions.h"
#include "hagl_utils.h"
#include <string.h>
#include <stdio.h>
#include "math_util.h"

static const char* _LOGGER_TAG = "demos";


void display_init() {
    ESP_LOGI(_LOGGER_TAG, "Initializing display...");
    hagl_init();
}

void display_deinit() {
    ESP_LOGI(_LOGGER_TAG, "Deinitializing display...");
    hagl_flush();
    hagl_close();
}

static color_t _get_random_color() {
    return rand() % 0xffff;
}

static vector_2d_t _get_centered_coordinates() {
    // mind screen rotation
    vector_2d_t coordinates = {
        .x = DISPLAY_WIDTH / 2,
        .y = DISPLAY_HEIGHT / 2
    };

    return coordinates;
}

static vector_2d_t _get_random_coordinates() {
    vector_2d_t coordinates = {
        .x = rand() % DISPLAY_WIDTH,
        .y = rand() % DISPLAY_HEIGHT
    };

    return coordinates;
}

void demo_draw_scaled_numbers(uint8_t start, uint8_t end) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Counting from %i to %i", start, end);

    const uint8_t scale_factor = 7;
    char number_buffer[get_int_length(end)];
    
    const unsigned char *font = font6x9;
    font_size_2d_t font_size = { .x = 6, .y = 9 };
    font_size_2d_t large_font_size = scale_font_size(font_size, scale_factor);

    vector_2d_t center = _get_centered_coordinates();
    
    //
    //   digit1   digit2   digit3
    // +--------+--------+--------+
    // |        |        |        |
    // |   +    |   +    |    +   |
    // |        |        |        |
    // +--------+--------+--------+
    // |   |        |         |   |
    // +---+    d   |    d    +---+
    //  d/2|        |          d/2
    //     |        |       
    //     +--------+
    //       offset
    //
    
    for (uint8_t i = start; i < end; i++) {
        itoa(i, number_buffer, 10);

        // calculate offset to screen center (see drawing above)
        int digits = get_int_length(i);
        const int d = large_font_size.x;
        const vector_2d_t base_offset = {
            .x = ((digits - 1) * d) / 2,
            .y = 0
        };

        ESP_LOGI(_LOGGER_TAG, "Rendering %s in character size %i", number_buffer, scale_factor);
        draw_scaled_text_with_center_origin(
            number_buffer, digits,
            center.x - base_offset.x,
            center.y - base_offset.y,
            _get_random_color(),
            font, font_size,
            scale_factor
        );
    }
}

void demo_blit_scaled_character(uint16_t times) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Displaying %i scaled characters", times);

    const unsigned char *font = font6x9;
    const font_size_2d_t font_size = { .x = 6, .y = 9 };
    vector_2d_t center = _get_centered_coordinates();

    for (uint16_t i = 0; i < times; i++) {
        ESP_LOGD(_LOGGER_TAG, "[%i/%i] Displaying scaled glyph demo.", i, times);

        char character = 'a' + (i % 26);
        draw_scaled_character_with_center_origin(
            character, 
            center.x, center.y,
            _get_random_color(),
            font, font_size, 
            7);
    }
    
    hagl_flush();
}

 void demo_draw_circles(uint16_t times) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Drawing %i circles", times);
    vector_2d_t center = _get_centered_coordinates();
    
    for (uint16_t i = 1; i < times; i++) {
        color_t color = _get_random_color();
        //int16_t radius = rand() % DISPLAY_WIDTH;

        hagl_draw_circle(center.x, center.y, i, color);
    }    
    hagl_flush();
}

 void demo_draw_bubbles(uint16_t times) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Drawing %i bubbles", times);
    for (uint16_t i = 1; i < times; i++) {
        vector_2d_t coordinates = _get_random_coordinates();
        int16_t radius = rand() % 100;
        color_t color = _get_random_color();

        hagl_fill_circle(coordinates.x, coordinates.y, radius, color);
    }
    hagl_flush();
}

 void demo_draw_ellipses(uint16_t times) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Drawing ellipses");
    vector_2d_t coordinates = _get_centered_coordinates();
    
    for (uint16_t i = 1; i < times; i++) {
        int16_t rx = rand() % DISPLAY_WIDTH;
        int16_t ry = rand() % DISPLAY_HEIGHT;
        color_t color = _get_random_color();

        hagl_draw_ellipse(coordinates.x, coordinates.y, rx, ry, color);
    }
    hagl_flush();
}

 void demo_draw_triangle(uint16_t times) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Drawing %i triangles", times);
    for (uint16_t i = 1; i < times; i++) {
        vector_2d_t coordinates1 = _get_random_coordinates();
        vector_2d_t coordinates2 = _get_random_coordinates(); 
        vector_2d_t coordinates3 = _get_random_coordinates();
        color_t color = _get_random_color();

        hagl_fill_triangle(
            coordinates1.x, coordinates1.y,
            coordinates2.x, coordinates2.y, 
            coordinates3.x, coordinates3.y, 
            color
        );
    }
    
    hagl_flush();
}

 void demo_draw_characters(uint16_t times) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Drawing %i random characters", times);
    vector_2d_t coordinates;
    for (uint16_t i = 1; i < times; i++) {
        coordinates = _get_random_coordinates();
        color_t color = _get_random_color();
        
        char code = rand() % 255;
        hagl_put_char(code, coordinates.x, coordinates.y, color, font6x9);
    }
    hagl_flush();
}

 void demo_draw_strings(uint16_t times) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Drawing %i strings", times);

    for (uint16_t i = 1; i < times; i++) {
        vector_2d_t coordinates = _get_random_coordinates();
        color_t color = _get_random_color();

        hagl_put_text(u"YOLO!", coordinates.x, coordinates.y, color, font6x9);
    }
    hagl_flush();
}

void demo_draw_scaled_texts(uint16_t times) {
    ESP_LOGI(_LOGGER_TAG, "DEMO: Drawing %i scaled strings", times);

    for (uint16_t i = 1; i < times; i++) {
        vector_2d_t coordinates = _get_random_coordinates();
        color_t color = _get_random_color();
        const font_size_2d_t font_size = { .x = 6, .y = 9 };

        draw_scaled_text_with_center_origin(
            "YOLO!", 6,
            coordinates.x, coordinates.y,
            color,
            font6x9, font_size,
            3
        );

        hagl_flush();
    }
}
