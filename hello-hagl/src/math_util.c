#include "math_util.h"

int get_int_length (int value){  
  // optimization for well known usecases
  if (value < 10) return 1;
  if (value < 100) return 2;
  if (value < 1000) return 3;
  
  // future proof
  int length = 1;
  for (; value != 0; value /= 10) {
    length++;
  }
  return length;
}