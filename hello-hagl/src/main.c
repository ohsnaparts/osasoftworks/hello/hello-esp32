#include "demos.h"
#include "esp_system.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>


static void delay(uint32_t milliseconds) {
    vTaskDelay(milliseconds / portTICK_RATE_MS);
}

void app_main() {
    display_init();

    demo_draw_ellipses(100);    
    demo_draw_characters(1200);
    demo_blit_scaled_character(5);
    demo_draw_scaled_numbers(1, 6);
    demo_draw_scaled_numbers(30, 35);
    demo_draw_scaled_numbers(103, 107);
    demo_draw_circles(DISPLAY_WIDTH+DISPLAY_WIDTH/2);
    demo_draw_bubbles(50);
    demo_draw_triangle(100);
    demo_draw_scaled_texts(30);
    demo_draw_strings(700);

    display_deinit();
}