#include "hagl_string_extensions.h"
#include "esp_log.h"
#include "hagl.h"

static const char* _LOGGER_TAG = "hagl_string_extensions";


font_size_2d_t scale_font_size(font_size_2d_t font_size, uint16_t scale_factor) {
    font_size_2d_t new_font_size = {
        .x = font_size.x * scale_factor,
        .y = font_size.y * scale_factor
    };

    return new_font_size;
}

void draw_scaled_text_with_center_origin(
    char* text, uint16_t text_length, 
    int x, int y, 
    color_t color,
    const unsigned char *font, font_size_2d_t font_size, 
    uint16_t scale_factor
) {
    // TODO: ensure only those characters are drawn that are actually visible on screen

    font_size_2d_t new_font_size = scale_font_size(font_size, scale_factor);
    
    char character;
    vector_2d_t coordinates;
    for (uint16_t i = 0; i < text_length; i++) {
        character = *(text + i);

        coordinates.x = x + new_font_size.x * i;
        coordinates.y = y;
        draw_scaled_character_with_center_origin(
            character, 
            coordinates.x, coordinates.y,
            color,
            font, font_size, 
            scale_factor
        );
    }
}

void draw_blitted_character(
    const char character, 
    vector_2d_t coordinates, 
    const unsigned char *font,
    font_size_2d_t font_size,
    font_size_2d_t new_font_size,
    color_t color
) {
    ESP_LOGD(_LOGGER_TAG, "Allocating bitmap buffer");
    bitmap_t bitmap; // dont forget to free mallocated space
    bitmap.buffer = (uint8_t *) malloc(font_size.x * font_size.y * sizeof(color_t));

    ESP_LOGD(_LOGGER_TAG, "Retrieving bitmap glyph for '%c'.", character);
    hagl_get_glyph(character, color, &bitmap, font);

    ESP_LOGD(_LOGGER_TAG, "Scaled rendering");
    hagl_scale_blit(
        coordinates.x, coordinates.y,
        new_font_size.x, new_font_size.y,
        &bitmap);

    hagl_flush();
    free(bitmap.buffer);
}

void draw_scaled_character_with_center_origin(
    const char character, 
    int x, int y, 
    color_t color,
    const unsigned char *font,
    font_size_2d_t font_size,
    uint16_t scale_factor
) {
    font_size_2d_t new_font_size = scale_font_size(font_size, scale_factor);

    // offset between top-left origin (0,0) and the center
    vector_2d_t font_center_offsets = {
        .x = new_font_size.x / 2,
        .y = new_font_size.y / 2
    };

    // shifts origin from top-left to center
    vector_2d_t coordinates = {
        .x = x - font_center_offsets.x,
        .y = y - font_center_offsets.y
    };

    draw_blitted_character(character, coordinates, font, font_size, new_font_size, color);
}