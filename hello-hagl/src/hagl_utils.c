#include "hagl_utils.h"
#include "esp_system.h"

color_t get_random_color() {
    return rand() % 0xffff;
}