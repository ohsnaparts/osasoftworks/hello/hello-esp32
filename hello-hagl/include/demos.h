#pragma once
#include "stdint.h"
#include "hagl_hal.h"
#include "hagl.h"
#include "vector.h"
#include "font_size.h"

/**
 * @brief Initializes and alloctes display resources
 */
void display_init();

/**
 * @brief Deallocates and tears down display resources
 */
void display_deinit();


/**
 * @brief Iterates the ASCII table starting with 'a' and renders it in large on the display
 * @param times how many letters to iterate
 */
void demo_blit_scaled_character(uint16_t times);

/**
 * @brief Draws a number of concentric circles to the screen
 * @param times The amount of circles to draw
 */
void demo_draw_circles(uint16_t times);

/**
 * @brief Draws a number of filled circles / bubbles to the screen.
 * @param times The amount of bubbles to draw
 */
void demo_draw_bubbles(uint16_t times);


/**
 * @brief Draws a number of ellipses to the screen
 * @param times The amount of ellipses to draw
 */
void demo_draw_ellipses(uint16_t times);

/**
 * @brief Draws a number of triangles to the screen
 * @param times The number of triangles to draw
 */
void demo_draw_triangle(uint16_t times);

/**
 * @brief 
 * Draws a number of glyphs to the screen
 * @param times The number of glyphs to draw
 */
void demo_draw_characters(uint16_t times);

/**
 * @brief Draws a number of strings to the screen
 * @param times The number of strings to draw
 */
void demo_draw_strings(uint16_t times);

/**
 * @brief Starts a large visual counter
 * @param start The number to start counting from
 * @param end The number at which the counter should stop
 */
void demo_draw_scaled_numbers(uint8_t start, uint8_t end);

/**
 * @brief Draws a number of scaled strings to the screen
 * @param times The number of strings to draw
 */
void demo_draw_scaled_texts(uint16_t times);
