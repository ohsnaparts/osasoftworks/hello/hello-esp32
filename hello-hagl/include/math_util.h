#pragma once

/**
 * @brief Calculates the digit count of an input integer
 * @param value The value to evaluate
 * @return int The amount of digits
 * @example get_int_length(643) => 3
 * @example get_int_length(11111) => 5
 */
int get_int_length (int value);