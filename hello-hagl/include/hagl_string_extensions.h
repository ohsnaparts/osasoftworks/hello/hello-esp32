#include "hagl.h"
#include "vector.h"
#include "font_size.h"
#include "stdint.h"


/**
 * @brief Scales up a font size by a specified factor
 * @param font_size source font size to scale
 * @param scale_factor scaling factor to multiply the font_size with
 * @return font_size_2d_t The scaled font size in pixels
 */
font_size_2d_t scale_font_size(font_size_2d_t font_size, uint16_t scale_factor);

/**
 * @brief Renders a specified text in scaled letters to the screen
 * 
 * @param text The text to display
 * @param text_length The length of the text to display
 * @param x The x coordinate of the first letter (origin: top-left)
 * @param y The y coordinate of the first letter (origin: top-left)
 * @param color The color to render the font in 
 * @param font A reference to a bitmap font
 * @param font_size The single character bitmap font size in pixel
 * @param scale_factor How much the source font should be elarged
 */
void draw_scaled_text_with_center_origin(
    char* text, uint16_t text_length, 
    int x, int y,
    color_t color,
    const unsigned char *font, font_size_2d_t font_size, 
    uint16_t scale_factor
);

/**
 * @brief Draws a single enlarged character to the screen
 * 
 * @param character character to enlarge
 * @param coordinates top-left origin (0,0)
 * @param font bitmap source font
 * @param font_size font size in pixels
 * @param new_font_size the new character font size
 * @param color color to draw the character in
 */
void draw_blitted_character(
    char character, 
    vector_2d_t coordinates, 
    const unsigned char *font,
    font_size_2d_t font_size,
    font_size_2d_t new_font_size,
    color_t color
);

/**
 * @brief Draws a scaled character at center origin.
 *        Center origin: coorinate (0,0) is assumed to be the bitmap center
 * 
 * @param character character to draw
 * @param x x coordinate in pixels
 * @param y x coordinate in pixels
 * @param font source bitmap font
 * @param font_size source bitmap pixel font size
 * @param scale_factor character enlargement factor (2 times, 3 times, n times)
 * @param color color to draw the character in
 */
void draw_scaled_character_with_center_origin(
    char character, 
    int x, int y, 
    color_t color,
    const unsigned char *font,
    font_size_2d_t font_size,
    uint16_t scale_factor
);