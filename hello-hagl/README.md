# Embedded Unit Testing

## Gist

Draws minimal shapes to a display using a hardware agnostic graphics layer.

![](./images/hello-hagl.gif)

## Problem

Fragmentation is intense the embedded world. Everyone cooks their own soup and creates single use-case libraries for 1 specific device without abstracting away common functionality. During my exploratij ghjkghjkghjkghjkghjkhjkon I've been using different libraries for Arduino, Meadow, ESP32, ESP32S2 Kaluga-1 where each would specialize in one display driver ST7789, ST7735, Waveshare e-paper.

Why not separate low level hardware specifics from high level logic. As a software developer, all I want to do is print pretty little dots and lines on my UI. Quite frankly, I don't particularly care about the low level details like screen type, display drivers, serial communication details, screen mirroring, coordinate offsets, color inversion...

## Solution

At first I created this layer myself through C++ interfaces and inheritance but found an amazing library
* [https://github.com/tuupola/hagl][hagl_github]

This library provides high level logic for drawing shapes to UI
* line
* circle
* ellipse
* triangle
* rectangle / rounded rectangle
* polygon
* char / string
* bitmap

This is does using a device specific hardware abstraction layer that implements the low level details on how to draw to a display.

* [https://github.com/tuupola/hagl_esp_mipi][hagl_hal_github]

The __minimal requirement__ of such a HAL is an override of a single function\
`hagl_hal_put_pixel(x0, y0, color)`

## Dependencies

2 [ESP-IDF components][pio_espidf_components]
* [hagl][hagl_github]
* [hagl_hal][hagl_hal_github]

Either fetch the repository recursively or update submodules after cloning.

## Setup

Platform: [PlatformIO][pio]

After cloning, the HAL layer will need to be configured to your display driver and environment needs. By default `menuconfig` includes menu extensions from `/components`.

> Kconfig support is [rather limited][pio_github_issue_453], so if, it isn't, try to rename the Kconfig file\
  `mv components/hagl_hal/Kconfig components/hagl_hal/Kconfig.projbuild`

> Note: The [HAL][ hagl_hal_github] repository contains default configurations for popular displays.

### Linux

If this is a fresh install of linux, you may have to install dependencies such as
```bash
# Testing suite is written in C++
sudo apt-get install g++
```
Testrunner output will tell you whats missing :)


### Windows

See https://gitlab.com/ohsnaparts/osasoftworks/zube/zube-app/-/issues/21

### PlatformIO Environment

To tell PlatformIO to test natively (not on the device iteself), a new environment needs to be creates

* [platformio.ini](./platformio.ini)


## Run

![Picture showing Platform IO "environment > native > test" sidebar button](https://gitlab.com/ohsnaparts/osasoftworks/zube/zube-app/uploads/4940c112a0279051bfb4ffc1bc6289f8/image.png)

```bash
pio test -e native
```

> By filtering tests for each environment, it is possible to run tests in different environments\
> This way it is possible to run native tests in CI/CD without having to have a device connected\
> See commit b17ae693aab2a14086c3e0483ee4ec021e5e8b3d


[pio]: https://platformio.org/
[hagl_hal_github]: https://github.com/tuupola/hagl_esp_mipi
[hagl_github]: https://github.com/tuupola/hagl
[pio_espidf_components]: https://docs.platformio.org/en/latest/frameworks/espidf.html#esp-idf-components
[pio_github_issue_453]: https://github.com/platformio/platform-espressif32/issues/453