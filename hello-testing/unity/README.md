# Embedded Unit Testing

Using [PlatformIO](https://platformio.org/)

## Dependencies

### Linux

If this is a fresh install of linux, you may have to install dependencies such as
```bash
# Testing suite is written in C++
sudo apt-get install g++
```
Testrunner output will tell you whats missing :)


### Windows

See https://gitlab.com/ohsnaparts/osasoftworks/zube/zube-app/-/issues/21

### PlatformIO Environment

To tell PlatformIO to test natively (not on the device iteself), a new environment needs to be creates

* [platformio.ini](./platformio.ini)


## Run

![Picture showing Platform IO "environment > native > test" sidebar button](https://gitlab.com/ohsnaparts/osasoftworks/zube/zube-app/uploads/4940c112a0279051bfb4ffc1bc6289f8/image.png)

```bash
pio test -e native
```

> By filtering tests for each environment, it is possible to run tests in different environments\
> This way it is possible to run native tests in CI/CD without having to have a device connected\
> See commit b17ae693aab2a14086c3e0483ee4ec021e5e8b3d

##
