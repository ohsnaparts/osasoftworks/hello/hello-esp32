#include <gtest/gtest.h>

// native
extern "C" int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    // if you plan to use GMock, replace the line above with
    // ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

// esp32
extern "C" int app_main(int argc, char **argv) {
    return main(argc, argv);
}