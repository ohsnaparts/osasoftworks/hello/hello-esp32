extern "C"
{
    #include "calculator_service.h"
    
    // include the exposed function declarations and definitions
    // so that we can override them with fake implementations 
    #include "calculator_service_globals.h"
}

#include "gtest/gtest.h"
#include "gtest/gtest-death-test.h"


// power() mock
// a more powerful option could be to use FFF for that
// https://github.com/meekrosoft/fff#a-fake-function-framework-for-c
static int _fake_power_result = 0;
static int _fake_power(int number, int power_number)
{
    return _fake_power_result;
}


namespace
{
    TEST(CalculatorService, invoke_double_power__exits_without_initialization)
    {
        #ifdef GTEST_HAS_DEATH_TEST
            EXPECT_EXIT(invoke_double_power(2, 2), ::testing::ExitedWithCode(6), "");
        #else
            GTEST_SKIP_("Death tests are unsupported by the current platform");
        #endif
    }

    TEST(CalculatorService, invokeDoublePower_usesRealPowerToCalculate) {
        init_calculator_service();
        EXPECT_EQ(invoke_double_power(2, 3), 16);   
    }

    TEST(CalculatorService, invokeDoublePower_usesFakePowerToCalculate)
    {
        init_calculator_service();
        // ensure that our power function / external dependency returns a static value
        // of our choosing. Have fun!
        _fake_power_result = -8;
        _calculator_service_power_func = _fake_power;
        EXPECT_EQ(invoke_double_power(0, 0), -16);
    }
}
