extern "C" {
    #include "power_service.h"
}

#include "gtest/gtest.h"

namespace {
    TEST(CalculationService, power) {
        EXPECT_EQ(8, power(2, 3));
    }
}