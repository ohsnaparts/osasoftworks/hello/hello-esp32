#include "main.h"
#include "calculator_service.h"
#include "logger.h"
#include <stdio.h>

// native main
int main() {
    log_information("Starting application!");
    init_calculator_service();

    char result_message[50];
     for ( int i = 0; i < 500 ; i++) {
        int result = invoke_double_power(i, i);
        snprintf(result_message, sizeof(result_message), "Received result: %i\n\n", result);
        log_information(result_message);
    }

    deinit_calculator_service();
}

// esp32 main
int app_main() {
    return main();
}