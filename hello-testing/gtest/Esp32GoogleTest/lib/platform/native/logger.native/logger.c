#include "stdio.h"

static void _log(char* level, char* text) {
    printf("Native [%s]: %s\n", level, text);
}

void log_information(char* text) { 
    _log("INFO", text); 
}

void log_debug(char* text) { 
    _log("DEBUG", text); 
}
