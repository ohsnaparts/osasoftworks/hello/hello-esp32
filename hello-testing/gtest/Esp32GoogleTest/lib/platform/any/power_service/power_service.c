#include "power_service.h"
#include "logger.h"
#include <stdio.h>

static void log_calculation(int number, int power, int result) {
    char log_message[50];
    snprintf(log_message, sizeof(log_message), "%i * %i = %i", number, power, result);
    log_debug(log_message);
}

// intentionally solving it this way to reproduce a
// challenge to be solved elsewhere
int power(int number, int power) {
    if (power == 0) { return 1; }
    
    int result = number;
    for (int i = 1; i < power; i++) {
        result *= number;
        log_calculation(number, i, result);
    }

    return result;
}