#pragma once

// Note that the service contract / interface has no concept of testing
// function pointers or anything the like. This is a hard requirement
// for inversion of control and liskov substitution!

void deinit_calculator_service();
void init_calculator_service();
int invoke_double_power(int number, int power);