#include "calculator_service.h"
#include "power_service.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "stdbool.h"

// include the function pointer declarations and definitions
#include "calculator_service_globals.h"


// Helper function to ensure that the program exits if the module
// has not been initialized (constructor not called)
static void _ensure_initializer_called() {
    bool is_initializer_called = _calculator_service_power_func != 0;
    if(!is_initializer_called) {
        errno = 6;
        printf("(errno %d) %s: %s\n", errno, strerror(errno), "Initializer has not been called.");
        exit(errno);
    }
}

// every time this module is freshly included, this needs to be called 
// to define all the function pointers to proper values.
void init_calculator_service() {
    _calculator_service_power_func = power;
}

void deinit_calculator_service() {
    _calculator_service_power_func = 0;
}

int invoke_double_power(int number, int power_number) {    
    _ensure_initializer_called();

    // using the power proxy function instead of the real implementation
    int single_power = _calculator_service_power_func(number, power_number);
    int double_power = single_power * 2;
    return double_power;
}