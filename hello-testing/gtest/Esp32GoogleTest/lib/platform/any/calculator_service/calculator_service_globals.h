#pragma once

#ifdef PIO_UNIT_TESTING
    // The PIO_UNIT_TESTING flag is set by the `pio test` command.
    // To increase encapsulation, the function pointer is declared as extern / global
    // only for unit tests. Thinking of C#, this could be compared to having an 
    // [InternalsVisibleTo("TestProjects")] for these function pointers.
    // Declaring the function pointer as extern allows us to modify the declaration
    // from other parts of the application. The unit test can then include this header 
    // to receive the declaration+definition that it can use to set fake implementations.
    extern int (*_calculator_service_power_func)(int, int);
#endif

int (*_calculator_service_power_func)(int, int);