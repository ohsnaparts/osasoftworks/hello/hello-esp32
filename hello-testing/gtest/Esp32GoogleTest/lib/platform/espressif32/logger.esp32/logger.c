#include "logger.h"
#include "esp_log.h"

void log_information(char* text) {
    ESP_LOGI("ESP32 Logger", "%s", text);
}

void log_debug(char* text) {
    ESP_LOGD("ESP32", "%s", text);
}
