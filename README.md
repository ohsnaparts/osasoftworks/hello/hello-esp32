# Introduction

This project acts as playground for experiments using ESP32-Microcontroller ([ESP-WROOM32](https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf) in particular) following the official [_Espressif programming guidex_](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html)

# Development Environment

* [Espressiv IoT Development Framework](https://github.com/espressif/esp-idf)
    * [_Setup instructions_](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html#setting-up-development-environment)
* [Visual Studio Code](https://code.visualstudio.com/)
    * [_Setup guide_](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/vscode-setup.html)

The setup too less then half an hour under windows, the instructions on docs.espressif.com are very well made.

1. Setup IDF (I think this step can be skipped when using the Visual Studio plugin)
1. Find COM-Serial port using Device Manager
1. Use Visual Studio Commands to create an ESP project / Use an example
1. Use VS-Code commands to "Build, flash and monitor" your device
