# Hello E-Ink

This project provides a minimal setup for drawing to an E-Ink display.

![demo-gif...](./images/hello-caleepd.gif)

# Hardware

* [Waveshare 2.7Inch e-Paper HAT](https://www.waveshare.com/2.7inch-e-Paper-HAT.htm)

# Dependencies

* [PlatformIO](https://platformio.org/)
* [martinberlin / CalEPD][github_martinberlin_calepd] Epaper ESP-IDF component with GFX capabilities and multi SPI support
    * [martinberlin / Adafruit-GFX-Library-ESP-IDF](https://github.com/martinberlin/Adafruit-GFX-Library-ESP-IDF) Adafruit GFX graphics core library forked so it compiles in ESP-IDF framework
    * [martinberlin / FT6X36-IDF](https://github.com/martinberlin/FT6X36-IDF) for gdew027w3T touch support
    * **Optional**
       * [EPDiy Driver](https://github.com/martinberlin/epdiy-rotation.git) for parallel displays

## Configuration

Use `menuconfig` to configure the right pinout for your display
* https://github.com/martinberlin/cale-idf/wiki/Model-gdew027w3T.h-(Touch)
   * ![](./images/caleepd-pinout_waveshare-27epd.jpg)
   * Be sure to doublecheck these pins with your configuration!

Additional configuration / code samples can be found in the [CaleIDF][github_martinberlin_caleidf] repository.

## Usage

1. Recursive git clone with submodules
1. Menuconfig
1. Wire up display according to configuration
1. Upload an Monitor

[github_martinberlin_calepd]: https://github.com/martinberlin/CalEPD
[github_martinberlin_caleidf]: https://github.com/martinberlin/cale-idf