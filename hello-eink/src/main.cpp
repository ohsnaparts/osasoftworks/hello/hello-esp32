#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <gdew027w3T.h>
#include "esp_log.h"

// INTGPIO is touch interrupt and goes low when it detects a touch, which coordinates are read by I2C
static FT6X36 touch_panel(CONFIG_TOUCH_INT);
static EpdSpi spi_io_interface;
static Gdew027w3T display(spi_io_interface, touch_panel);
static const bool cleanScreenAtStart = true;
static const char* _LOGGER_TAG = "main";

extern "C"
{
   void app_main();
}


static void draw_lines(bool autoflush = false) {
    ESP_LOGI(_LOGGER_TAG, "Drawing lines...");

    for ( uint16_t height = GDEW027W3_HEIGHT; height > 0; height-=2) {
        int x1 = 0, y1 = height;
        int x2 = GDEW027W3_WIDTH, y2 = height;
        
        ESP_LOGD(_LOGGER_TAG, "   (%i,%i)->(%i,%i)", x1, x2, y1, y2);
        display.drawLine(
            x1, y1, 
            x2, y2,
            EPD_BLACK
        );
    }

    if (autoflush) {
        ESP_LOGD(_LOGGER_TAG, "   Flushing lines to display");
        display.update();
    }
}

static void draw_circles(bool autoflush = false) {
    ESP_LOGI(_LOGGER_TAG, "Drawing circles...");
    const int16_t center_x = GDEW027W3_WIDTH / 2;
    const int16_t center_y = GDEW027W3_HEIGHT /2;

    for (int16_t radius = GDEW027W3_HEIGHT; radius > 0; radius -= 5) {
        ESP_LOGD(_LOGGER_TAG, "   circle (x%i,y%i)-> r%i", center_x, center_y, radius);
        display.drawCircle(center_x, center_y, radius, EPD_BLACK);
    }

    if (autoflush) {
        ESP_LOGD(_LOGGER_TAG, "   Flushing circles to display");
        display.update();
    }
}

void app_main(void)
{
    ESP_LOGI(_LOGGER_TAG, "Initializing E-Ink display...");
    display.init(false);

    if (cleanScreenAtStart) {
        ESP_LOGD(_LOGGER_TAG, "Clearing screen...");
        display.update();
    }

    draw_lines(true);
    draw_circles(true);
}
