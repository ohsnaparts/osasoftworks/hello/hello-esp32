#pragma once
#include "stdint.h"

typedef struct
{
    uint16_t r;
    uint16_t g;
    uint16_t b;
} color_t;

typedef uint16_t rgb565_t;

rgb565_t to_rgb565color_(const color_t color);
rgb565_t to_rgb565color(
    const uint16_t red,
    const uint16_t green,
    const uint16_t blue);

rgb565_t get_random_rgb565_color();
color_t get_random_rgb_color();