#include "display_colors.h"
#include "esp_system.h"
#include "st7789.h"

// Proxy to not rely too heavily on ESP functionality
static uint32_t _random_number()
{
    return esp_random();
}

rgb565_t to_rgb565color(uint16_t red, uint16_t green, uint16_t blue)
{
    return rgb565_conv(red, green, blue);
}

rgb565_t to_rgb565color_(color_t color)
{
    return to_rgb565color(color.r, color.g, color.b);
}

color_t get_random_rgb_color()
{
    const color_t color = {
        .r = _random_number() % 255,
        .g = _random_number() % 255,
        .b = _random_number() % 255};
    return color;
}

rgb565_t get_random_rgb565_color()
{
    return to_rgb565color_(get_random_rgb_color());
}