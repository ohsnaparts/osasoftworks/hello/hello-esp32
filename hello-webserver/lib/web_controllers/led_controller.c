#include "led_controller.h"
#include "esp_onboard_temp.h"
#include "esp_log.h"
#include "cJSON.h"
#include "string.h"
#include "ledc.h"

static const char *_LOGGER_TAG = "led_controller";

#define REQUEST_BODY_BUFFER_SIZE 64
#define REQUEST_ERROR_MESSAGE_BUFFER_SIZE 128

static esp_err_t POST_set_led_color_handler(httpd_req_t *request);

static int8_t _leds_size = -1;
static struct ledc_rgb_led_t *_leds;

static const struct led_controller_routes_t _routes = {
    .POST_set_led_color = {
        .uri = "/led",
        .method = HTTP_POST,
        .handler = POST_set_led_color_handler,
    }};

const struct led_controller_routes_t *get_led_controller_routes()
{
    return &_routes;
}

static bool is_initialized()
{
    return _leds_size > -1;
}

esp_err_t led_controller_init(struct ledc_rgb_led_t *leds, int8_t leds_size)
{
    ESP_LOGI(_LOGGER_TAG, "Initializing...");
    if (is_initialized())
    {
        ESP_LOGE(_LOGGER_TAG, "Already initialized. Skipping...");
        return ESP_FAIL;
    }

    _leds = leds;
    _leds_size = leds_size;
    return ESP_OK;
}

/**
 * @brief sets led color using request json payload
 * 
 * @param request { "r" : 256.00 ,"g" : 256.00 , "b" : 256.00 }
 * @return esp_err_t ESP_OK, ESP_FAIL
 */
static esp_err_t POST_set_led_color_handler(httpd_req_t *request)
{
    ESP_LOGI(_LOGGER_TAG, "Received request with length %i", request->content_len);

    char body[REQUEST_BODY_BUFFER_SIZE];
    char error_message[REQUEST_ERROR_MESSAGE_BUFFER_SIZE];
    memset(body, 0, sizeof(body));
    memset(error_message, 0, sizeof(error_message));

    cJSON *json = try_get_json_request(request, body, sizeof(body), error_message, sizeof(error_message));
    if (json == NULL)
    {
        ESP_ERROR_CHECK(httpd_resp_send(request, error_message, HTTPD_RESP_USE_STRLEN));
        return HTTPD_500_INTERNAL_SERVER_ERROR;
    }

    ESP_LOGD(_LOGGER_TAG, "Retrieving led request information values from json message");
    int r = cJSON_GetObjectItem(json, "r")->valueint;
    int g = cJSON_GetObjectItem(json, "g")->valueint;
    int b = cJSON_GetObjectItem(json, "b")->valueint;
    int led_id = cJSON_HasObjectItem(json, "led_id")
                     ? cJSON_GetObjectItem(json, "led_id")->valueint
                     : 0;

    cJSON_Delete(json);

    ESP_LOGI(_LOGGER_TAG, "Requested the setting of LED %i of %i to RGB %i,%i,%i", led_id, _leds_size, r, g, b);
    if (led_id >= _leds_size)
    {
        httpd_resp_send(request, "Specified led id is out of bounds.", HTTPD_RESP_USE_STRLEN);
        return ESP_FAIL;
    }

    if (!_leds[led_id].is_initialized)
    {
        httpd_resp_send(request, "Invalid led id received. Specified led is uninitialized.", HTTPD_RESP_USE_STRLEN);
        return ESP_FAIL;
    }

    set_led_color_percent(&_leds[led_id], r, g, b, _leds[led_id].fade_milliseconds);

    httpd_resp_send(request, body, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}