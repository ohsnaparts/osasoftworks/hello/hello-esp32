#include "lcd_controller.h"
#include "esp_log.h"
#include "display.h"
#include "display_colors.h"

// Prototypes
static esp_err_t GET_fill_screen(httpd_req_t *request);
static esp_err_t POST_draw_text(httpd_req_t *request);

// Globals
static char *_LOGGING_TAG = "lcd_controller";
static lcd_controller_routes_t _routes = {
    .GET_fill_screen = {.uri = "/fill", .method = HTTP_GET, .handler = &GET_fill_screen},
    .POST_draw_text = {.uri = "/text", .method = HTTP_POST, .handler = &POST_draw_text}};

esp_err_t lcd_controller_init()
{
    ESP_LOGI(_LOGGING_TAG, "Initializing ldc_controller...");
    return ESP_OK;
}

const lcd_controller_routes_t *get_lcd_controller_routes()
{
    return &_routes;
}

/**
 * @brief Renders text to the screen
 * @param request a POST request with json payload
 *                { "text": "string", position: { "x": number, "y": number } }
 * @return httpd_err_code_t
 */
static esp_err_t POST_draw_text(httpd_req_t *request)
{
    char payload[128];
    char error_message[128];

    cJSON *json = try_get_json_request(request, payload, sizeof(payload), error_message, sizeof(error_message));
    if (json == NULL)
    {
        httpd_resp_set_status(request, HTTPD_500_INTERNAL_SERVER_ERROR);
        ESP_ERROR_CHECK_WITHOUT_ABORT(httpd_resp_send(request, error_message, HTTPD_RESP_USE_STRLEN));
        return ESP_FAIL;
    }

    uint16_t posX = 0, posY = 0;
    if (cJSON_HasObjectItem(json, "position"))
    {
        cJSON *position = cJSON_GetObjectItem(json, "position");
        if (cJSON_HasObjectItem(position, "x"))
            posX = (uint16_t)cJSON_GetObjectItem(position, "x")->valueint;
        if (cJSON_HasObjectItem(position, "y"))
            posY = (uint16_t)cJSON_GetObjectItem(position, "y")->valueint;
    }

    char *text = cJSON_HasObjectItem(json, "text")
                     ? cJSON_GetObjectItem(json, "text")->valuestring
                     : "";

    rgb565_t black = to_rgb565color(255, 255, 255);
    rgb565_t white = to_rgb565color(0, 0, 0);
    draw_string(posX, posY, black, white, text);

    cJSON_Delete(json);

    httpd_resp_send(request, "OK", HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

/**
 * @brief Fills screen with random color
 * @param request an empty GET request via the browser
 * @return httpd_err_code_t
 */
static esp_err_t GET_fill_screen(httpd_req_t *request)
{
    color_t color = get_random_rgb_color();
    fill_screen(to_rgb565color_(color));

    char response[128];
    memset(response, 0, sizeof(response));

    char *response_pattern = "<html><body><span style = 'color: rgb(%i,%i,%i)'>Color filled with RGB(%i, %i, %i)</span></body><html> ";
    if (snprintf(
            response,
            sizeof(response),
            response_pattern,
            color.r, color.g, color.b,
            color.r, color.g, color.b) < 0)
    {
        ESP_LOGE(_LOGGING_TAG, "An error occured while building response of pattern %s.", response_pattern);
        ESP_LOGE(_LOGGING_TAG, "Unable to build response. Failing request.");
        ESP_ERROR_CHECK_WITHOUT_ABORT(httpd_resp_send_500(request));
        return ESP_FAIL;
    }

    if (ESP_ERROR_CHECK_WITHOUT_ABORT(
            httpd_resp_send(request, response, HTTPD_RESP_USE_STRLEN)) != ESP_OK)
    {
        ESP_LOGE(_LOGGING_TAG, "An error occured while sending response: %s", response);
        ESP_ERROR_CHECK_WITHOUT_ABORT(httpd_resp_send_500(request));
        return ESP_FAIL;
    };

    return ESP_OK;
}