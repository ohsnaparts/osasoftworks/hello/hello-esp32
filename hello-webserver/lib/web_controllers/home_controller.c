#include "home_controller.h"
#include "esp_onboard_temp.h"
#include "esp_log.h"

static const char *_LOGGER_TAG = "home_controller";

static esp_err_t GET_hello_handler(httpd_req_t *req);
static esp_err_t GET_temperature_handler(httpd_req_t *request);

static const httpd_uri_t _get_hello = {
    .uri = "/hello",
    .method = HTTP_GET,
    .handler = GET_hello_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx = "Hello World!"};

static const httpd_uri_t _get_temperature = {
    .uri = "/temperature",
    .method = HTTP_GET,
    .handler = GET_temperature_handler,
    .user_ctx = NULL};

static const struct home_controller_endpoints_t _home_controller_endpoints = {
    .get_hello = &_get_hello,
    .get_temperature = &_get_temperature};

const struct home_controller_endpoints_t *get_home_controller_endpoints()
{
    return &_home_controller_endpoints;
}

static esp_err_t GET_hello_handler(httpd_req_t *request)
{
    /* Send a simple response */
    const char response[] = "Sup!";
    httpd_resp_send(request, response, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

static esp_err_t GET_temperature_handler(httpd_req_t *request)
{
    ESP_ERROR_CHECK(init_temperature_sensor());

    float celsius;
    char response[10] = {'0', '0', '0', '.', '0', '0', '0', '0', '0', '0'};

    get_oboard_temperature(&celsius);
    ESP_LOGI(_LOGGER_TAG, "Current onboard temperature: %f celsius", celsius);

    int ret = snprintf(response, sizeof(response), "%f", celsius);
    if (ret < 0)
    {
        ESP_LOGE(_LOGGER_TAG, "An error occured while truncating float %f to size %i", celsius, sizeof(response));
        ESP_LOGE(_LOGGER_TAG, "Unable to build response. Failing request.");
        return ESP_FAIL;
    }

    return httpd_resp_send(request, response, HTTPD_RESP_USE_STRLEN);
}