#include "webserver.h"

struct home_controller_endpoints_t
{
    const httpd_uri_t *get_hello;
    const httpd_uri_t *get_temperature;
};

const struct home_controller_endpoints_t *get_home_controller_endpoints();