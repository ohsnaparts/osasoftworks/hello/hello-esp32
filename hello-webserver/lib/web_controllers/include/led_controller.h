#include "webserver.h"
#include "ledc.h"

struct led_controller_routes_t
{
    const httpd_uri_t POST_set_led_color;
};

esp_err_t led_controller_init(struct ledc_rgb_led_t *leds, int8_t leds_size);
const struct led_controller_routes_t *get_led_controller_routes();