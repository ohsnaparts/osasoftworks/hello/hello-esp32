#pragma once
#include "webserver.h"
#include "display.h"

typedef struct
{
    const httpd_uri_t GET_fill_screen;
    const httpd_uri_t POST_draw_text;
} lcd_controller_routes_t;

esp_err_t lcd_controller_init();
const lcd_controller_routes_t *get_lcd_controller_routes();