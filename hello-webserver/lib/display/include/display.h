#pragma once
#include "display_colors.h"
#include "vector.h"

void fill_screen(const rgb565_t color);

void draw_string(
    uint16_t position_x,
    uint16_t position_y,
    rgb565_t color_foreground,
    rgb565_t color_background,
    char *text);

void draw_rectangle(
    const uint16_t position_x,
    const uint16_t position_y,
    const uint16_t size_x,
    const uint16_t size_y,
    const rgb565_t color);

void draw_square(
    const uint16_t position_x,
    const uint16_t position_y,
    const uint16_t size,
    const rgb565_t color);

void initialize_display(
    const int16_t gpio_mosi,
    const int16_t gpio_sclk,
    const int16_t gpio_cs,
    const int16_t gpio_dc,
    const int16_t gpio_reset,
    const int16_t gpio_bl,
    const int16_t size_x,
    const int16_t size_y,
    const int16_t offset_x,
    const int16_t offset_y,
    const char *font_path);