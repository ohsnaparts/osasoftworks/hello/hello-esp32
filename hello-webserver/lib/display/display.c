#include "st7789.h"
#include "esp_vfs.h"
#include "display.h"
#include "esp_system.h"
#include "esp_log.h"

static TFT_t _display;
static FontxFile _font[2];
static const char *_LOGGER_TAG = "display";

static void _initialize_font(const char *font_path)
{
    InitFontx(_font, font_path, ""); // 16x32Dot Gothic

    // sizes initialized on open =>
    OpenFontx(&_font[0]);
    CloseFontx(&_font[0]);
}

void initialize_display(
    int16_t gpio_mosi,
    int16_t gpio_sclk,
    int16_t gpio_cs,
    int16_t gpio_dc,
    int16_t gpio_reset,
    int16_t gpio_bl,
    int16_t size_x,
    int16_t size_y,
    int16_t offset_x,
    int16_t offset_y,
    const char *font_path)
{
    spi_master_init(
        &_display,
        gpio_mosi,
        gpio_sclk,
        gpio_cs,
        gpio_dc,
        gpio_reset,
        gpio_bl);
    lcdInit(
        &_display,
        size_x,
        size_y,
        offset_x,
        offset_y);

    lcdInversionOn(&_display);
    if (font_path != NULL)
    {
        _initialize_font(font_path);
    }
}

static bool _assert_fonts_initalized()
{
    return _font[0].valid || _font[1].valid;
}

void draw_string(
    uint16_t position_x,
    uint16_t position_y,
    rgb565_t color_foreground,
    rgb565_t color_background,
    char *text)
{
    if (!_assert_fonts_initalized())
    {
        ESP_LOGE(_LOGGER_TAG, "Unable to draw string with uninitialized fonts.");
        return;
    }
    lcdSetFontFill(&_display, color_background);

    unsigned char *unsigned_text = (unsigned char *)text;
    ESP_LOGI(_LOGGER_TAG, "Drawing string: %s", unsigned_text);

    lcdDrawString(
        &_display,
        _font,
        position_x,
        position_y,
        unsigned_text,
        color_foreground);
}

void fill_screen(const rgb565_t color)
{
    ESP_LOGI(_LOGGER_TAG, "Filling screen with color %i", color);
    lcdFillScreen(&_display, color);
}

void draw_square(
    const uint16_t position_x,
    const uint16_t position_y,
    const uint16_t size,
    const rgb565_t color)
{
    draw_rectangle(
        position_x,
        position_y,
        size,
        size,
        color);
}

void draw_rectangle(
    const uint16_t position_x,
    const uint16_t position_y,
    const uint16_t size_x,
    const uint16_t size_y,
    const rgb565_t color)
{

    const uint16_t position_x2 = position_x + size_x;
    const uint16_t position_y2 = position_y + size_y;
    lcdDrawFillRect(
        &_display,
        position_x, position_y,
        position_x2, position_y2,
        color);
}