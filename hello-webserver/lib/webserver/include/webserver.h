#include "esp_err.h"
#include "esp_http_server.h"
#include "cJSON.h"

esp_err_t read_request_body(
    httpd_req_t *request,
    char *body_buffer, uint8_t body_buffer_size,
    char *error_buffer, uint8_t error_buffer_size);

esp_err_t stop_webserver();
esp_err_t start_webserver();
esp_err_t register_endpoint(const httpd_uri_t *endpoint);

esp_err_t try_get_text_request(
    httpd_req_t *request,
    char *payload_buffer, uint8_t payload_buffer_size,
    char *error_buffer, uint8_t error_buffer_size);

cJSON *try_get_json_request(
    httpd_req_t *request,
    char *payload_buffer, uint8_t payload_buffer_size,
    char *error_buffer, uint8_t error_buffer_size);
