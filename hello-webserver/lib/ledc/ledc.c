#include "ledc.h"
#include "esp_log.h"
#include "math_util.h"
#include <math.h>

// Prototypes
static int _calculate_duty(const struct ledc_led_t *led, int8_t percent);
static void _set_color_soft(const ledc_channel_config_t *channel, int32_t duty, int32_t fade_time_ms);
static void _set_color_hard(const ledc_channel_config_t *channel, int32_t duty);
static void _set_led_color(const struct ledc_led_t *led, int32_t duty, int32_t fade_time_ms);
static void _set_rgb_led_color(
    const struct ledc_rgb_led_t *led,
    int32_t duty_red,
    int32_t duty_green,
    int32_t duty_blue,
    int32_t fade_time_ms);

// Globals
static const char *TAG = "ledc";

int get_max_duty(const ledc_timer_config_t *timer)
{
    int duty_resolution = (int)timer->duty_resolution;
    int duty = pow(2, duty_resolution) - 1;

    ESP_LOGD(TAG, "Calculated max duty of %d with a resolution of %d", duty, duty_resolution);
    return duty;
}

static void _set_led_color(
    const struct ledc_led_t *led,
    int32_t duty,
    int32_t fade_time_ms)
{

    if (fade_time_ms > 0)
    {
        _set_color_soft(&led->channel, duty, fade_time_ms);
    }
    else
    {
        _set_color_hard(&led->channel, duty);
    }
}

static void _set_rgb_led_color(
    const struct ledc_rgb_led_t *led,
    int32_t duty_red,
    int32_t duty_green,
    int32_t duty_blue,
    int32_t fade_time_ms)
{
    _set_led_color(&led->red, duty_red, fade_time_ms);
    _set_led_color(&led->green, duty_green, fade_time_ms);
    _set_led_color(&led->blue, duty_blue, fade_time_ms);
}

static int _calculate_duty(const struct ledc_led_t *led, int8_t percent)
{
    int max_duty = get_max_duty(&led->timer);
    int duty = floor((max_duty / (float)100) * percent);
    duty = ranged_value(duty, 0, max_duty);

    ESP_LOGD(TAG, "Calculated duty %d for percent %d", duty, percent);
    return duty;
}

void set_led_color_percent(
    const struct ledc_rgb_led_t *led,
    int percent_red,
    int percent_green,
    int percent_blue,
    int32_t transition_interval_ms)
{
    percent_red = ranged_value(percent_red, 0, 100);
    percent_green = ranged_value(percent_green, 0, 100);
    percent_blue = ranged_value(percent_blue, 0, 100);
    
    if ( led->is_common_anode) {
        // less gpio backpressure = more flow through the led
        percent_red = 100 - percent_red;
        percent_green = 100 - percent_green;
        percent_blue = 100 - percent_blue;
    }

    int duty_red = _calculate_duty(&led->red, percent_red);
    int duty_green = _calculate_duty(&led->green, percent_green);
    int duty_blue = _calculate_duty(&led->blue, percent_blue);

    _set_rgb_led_color(
        led,
        duty_red,
        duty_green,
        duty_blue,
        transition_interval_ms);
}

static void _set_color_hard(const ledc_channel_config_t *channel, int32_t duty)
{
    if (duty < 0)
        return;

    // configure channel updates
    ESP_LOGD(TAG, "Switching duty cycle of channel %d from %d => %d", channel->channel, channel->duty, duty);
    ESP_ERROR_CHECK(ledc_set_duty(
        channel->speed_mode,
        channel->channel,
        duty));

    // activate changes
    ESP_ERROR_CHECK(ledc_update_duty(channel->speed_mode, channel->channel));
}

static void _set_color_soft(const ledc_channel_config_t *channel, int32_t duty, int32_t fade_time_ms)
{
    if (duty < 0)
        return;

    ESP_LOGD(TAG, "Softly setting duty cycle of channel %d from %d => %d over %d ms",
             channel->channel, channel->duty, duty, fade_time_ms);
    ESP_ERROR_CHECK(ledc_set_fade_with_time(channel->speed_mode, channel->channel, duty, fade_time_ms));
    ESP_ERROR_CHECK(ledc_fade_start(channel->speed_mode, channel->channel, LEDC_FADE_NO_WAIT));
}

void configure_led(const struct ledc_led_t *led)
{
    ESP_ERROR_CHECK(ledc_timer_config(&led->timer));
    ESP_ERROR_CHECK(ledc_channel_config(&led->channel));
}

void configure_rgb_led(const struct ledc_rgb_led_t *led)
{
    configure_led(&led->red);
    configure_led(&led->green);
    configure_led(&led->blue);

    ledc_fade_func_install(0);
}
