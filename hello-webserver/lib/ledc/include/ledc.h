#pragma once
#include "driver/ledc.h"

struct ledc_led_t
{
    char name[10];
    ledc_channel_config_t channel;
    ledc_timer_config_t timer;
};

struct ledc_rgb_led_t
{
    bool is_initialized;
    bool is_common_anode;
    char name[10];
    struct ledc_led_t red;
    struct ledc_led_t green;
    struct ledc_led_t blue;
    int32_t fade_milliseconds;
};

int get_max_duty(const ledc_timer_config_t *timer);
esp_err_t register_ledc_led(int id, const struct ledc_rgb_led_t *led);

void configure_rgb_led(const struct ledc_rgb_led_t *led);
void configure_led(const struct ledc_led_t *led);

void set_led_color_percent(
    const struct ledc_rgb_led_t *led,
    int percent_red,
    int percent_green,
    int percent_blue,
    int transition_interval_ms);
