#include "esp_onboard_temp.h"
#include "esp_log.h"
#include "esp_err.h"

static const char *_LOGGER_TAG = "onboard_temperature";
static bool _is_initialized = false;

esp_err_t init_temperature_sensor()
{
    if (_is_initialized)
    {
        ESP_LOGW(_LOGGER_TAG, "Trying to reinitialize temperature sensor. Skipping.");
        return ESP_OK;
    }

    ESP_LOGI(_LOGGER_TAG, "Initializing omboard temperature sensor...");

#if !HAS_ONBOARD_TEMPERATURE_SUPPORT
    ESP_LOGE(_LOGGER_TAG, "Onboard temperature sensing not supported by your device.");
    ESP_LOGE(_LOGGER_TAG, "Be sure to set idf target device properly.");
    ESP_LOGE(_LOGGER_TAG, "Currently only ESP32S2 and ESP32C3 based devices are supported.");
    return ESP_FAIL;
#else
    temp_sensor_config_t temp_sensor_config = TSENS_CONFIG_DEFAULT();
    temp_sensor_get_config(&temp_sensor_config);

    ESP_LOGD(_LOGGER_TAG, "Temp_sensor default dac %d, clk_div %d",
             temp_sensor_config.dac_offset, temp_sensor_config.clk_div);

    // defines the temperature range and accuracy => see enum comments
    temp_sensor_config.dac_offset = TSENS_DAC_DEFAULT;

    ESP_ERROR_CHECK(temp_sensor_set_config(temp_sensor_config));

    _is_initialized = true;
    return ESP_OK;
#endif
}

esp_err_t deinit_temperature_sensor()
{
    _is_initialized = false;

#if HAS_ONBOARD_TEMPERATURE_SUPPORT
    return temp_sensor_stop();
#endif

    return ESP_OK;
}

esp_err_t get_oboard_temperature(float *out_celsius)
{
    int error_fallback_temperature = 0;
#if !HAS_ONBOARD_TEMPERATURE_SUPPORT
    ESP_LOGE(_LOGGER_TAG, "Onboard temperature sensing not supported by your device.");
    ESP_LOGE(_LOGGER_TAG, "Be sure to set idf target device properly.");
    ESP_LOGE(_LOGGER_TAG, "Currently only ESP32S2 and ESP32C3 based devices are supported.");
    ESP_LOGE(_LOGGER_TAG, "Returning fallback temperature %i", error_fallback_temperature);
    *out_celsius = error_fallback_temperature;
    return ESP_FAIL;
#else
    ESP_ERROR_CHECK(temp_sensor_start());

    esp_err_t measure_state = ESP_OK;
    float celsius;

    measure_state = temp_sensor_read_celsius(&celsius);

    *out_celsius = measure_state == ESP_OK
                       ? celsius
                       : error_fallback_temperature;

    ESP_ERROR_CHECK(temp_sensor_stop());

    ESP_LOGD(_LOGGER_TAG, "Onboard temperature: %f", celsius);
    return measure_state;
#endif
}