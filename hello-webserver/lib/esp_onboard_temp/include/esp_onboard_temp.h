#include "freertos/FreeRTOS.h"
#include "stdint.h"

// Only include on devices that support / have a onboard temperature sensor
// In case this is an error, please doublecheck documentation and if needed adjust
// both checks in headerfile and implementation to support the new device :)
#define HAS_ONBOARD_TEMPERATURE_SUPPORT CONFIG_IDF_TARGET_ESP32S2 || CONFIG_IDF_TARGET_ESP32C3

#if HAS_ONBOARD_TEMPERATURE_SUPPORT

#include "driver/temp_sensor.h"

#endif

esp_err_t deinit_temperature_sensor();
esp_err_t init_temperature_sensor();
esp_err_t get_oboard_temperature(float *out_celsius);