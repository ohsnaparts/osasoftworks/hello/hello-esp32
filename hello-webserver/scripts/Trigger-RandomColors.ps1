﻿$Script:IntervalCount = [int]::MaxValue;
$Script:IntervalSeconds = 5;
$Script:BaseUrl = 'http://192.168.8.117';
$Script:Method = [Microsoft.PowerShell.Commands.WebRequestMethod]::Post


Function Draw-DisplayText {
    [CmdletBinding()]    
    Param(
        [Parameter(ValueFromPipelineByPropertyName)]
        [string]$Text,
        [Parameter(ValueFromPipelineByPropertyName)]
        [int]$PositionX,
        [Parameter(ValueFromPipelineByPropertyName)]
        [int]$PositionY
    )
    $Uri = [Uri]::new("$Script:BaseUrl/text");

    $Body = @{ 
        text = $Text; 
        position = @{
            x = $PositionX;
            y = $PositionY;
        }
    } | ConvertTo-Json -Compress;

    $Prefix = "[$PositionX,$PositionY]";
    Write-Host ("{0} > {1}" -f $Prefix, $Body);
    Write-Host ("{0} < " -f $Prefix)  -NoNewline;
    $Body | Invoke-RestMethod -Method $Script:Method -Uri $Uri -TimeoutSec 10 
}

Function Set-RgbLedColor {
    [CmdletBinding()]    
    Param(
        [Parameter(ValueFromPipelineByPropertyName)]
        [int]$Red,
        [Parameter(ValueFromPipelineByPropertyName)]
        [int]$Green,
        [Parameter(ValueFromPipelineByPropertyName)]
        [int]$Blue,
        [int]$LedId,
        [switch] $CommonAnode
    )

    $Uri = [Uri]::new("$Script:BaseUrl/led");

    if ( $CommonAnode ) {
        $Red = 100 - $Red;
        $Green = 100 - $Green;
        $Blue = 100 - $Blue;
    }

    $Body = @{ 
        r = $Red; 
        g = $Green; 
        b = $Blue; 
        led_id = $LedId } | ConvertTo-Json -Compress;

    Write-Host "Led $LedId > $Body";
    Write-Host -NoNewline "Led $LedId < ";
    $Body | Invoke-RestMethod -Method $Script:Method -Uri $Script:Url -TimeoutSec 10
}


Function New-LedColor {
    PARAM(
        [Parameter(ValueFromPipelineByPropertyName)]
        [int]$Red,
        [Parameter(ValueFromPipelineByPropertyName)]
        [int]$Green,
        [Parameter(ValueFromPipelineByPropertyName)]
        [int]$Blue
    )

    [PSCustomObject] @{
        Red = $Red;
        Green = $Green;
        Blue = $Blue;
   }
}


Function Get-RandomColor {
    New-LedColor `
        -Red $(Get-Random -Minimum 0 -Maximum 100) `
        -Green $(Get-Random -Minimum 0 -Maximum 100) `
        -Blue $(Get-Random -Minimum 0 -Maximum 100)
}


Function Set-PartyLedColors {
    Get-RandomColor | Set-RgbLedColor -LedId 0 -CommonAnode:$CommonAnode
    Get-RandomColor | Set-RgbLedColor -LedId 1 -CommonAnode:$CommonAnode
}

Function Set-MonoLedColors {
    $Color = Get-RandomColor
    $Color | Set-RgbLedColor -LedId 0 -CommonAnode:$CommonAnode
    $Color | Set-RgbLedColor -LedId 1 -CommonAnode:$CommonAnode
}


$Colors = @{
    Red = New-LedColor -Red 100 -Green 0 -Blue 0;
    Green = New-LedColor -Red 0 -Green 100 -Blue 0;
    Blue = New-LedColor -Red 0 -Green 0 -Blue 100;
}

# Intense colors
$OffsetY=2
$FontSizePx=29
$ScreenHeightPx=320
$MaxLines = $ScreenHeightPx / $FontSizePx
$Counter=0
While ( $True ) {
    $Colors.Values | Get-Random | Set-RgbLedColor -LedId 0 -CommonAnode
    $Colors.Values | Get-Random | Set-RgbLedColor -LedId 1 -CommonAnode
    
    $Counter = $Counter++ % $MaxLines
    $TextPosY = $OffsetY + ($Counter++ * $FontSizePx)
    Draw-DisplayText -Text $(Get-Date -UFormat "%s") -PositionX 0 -PositionY $TextPosY    
    Sleep -Seconds 10
}

<#
Write-Host "Invoking $Method $Url $Script:IntervalCount times in intervals of $Script:IntervalSeconds seconds"
1..$Script:IntervalCount | ForEach-Object {
    
   Set-PartyLedColors
   #Set-MonoLedColors

   Start-Sleep -Seconds $Script:IntervalSeconds;
}
#>

<#
    $Colors.Green | Set-RgbLedColor -LedId 0 -CommonAnode
    $Colors.Green | Set-RgbLedColor -LedId 1 -CommonAnode
    Sleep -Seconds 10

    $Colors.Blue | Set-RgbLedColor -LedId 0 -CommonAnode
    $Colors.Blue | Set-RgbLedColor -LedId 1 -CommonAnode

    $Colors.Red | Set-RgbLedColor -LedId 0 -CommonAnode
    $Colors.Red | Set-RgbLedColor -LedId 1 -CommonAnode
    Sleep -Seconds 5
    #>