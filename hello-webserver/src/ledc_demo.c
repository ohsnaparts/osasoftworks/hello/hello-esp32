#include "ledc_demo.h"
#include "ledc_init.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "ledc.h"

static const char* _LOGGING_TAG = "ledc_demo";
static struct ledc_rgb_led_t *_leds;
static uint8_t _leds_size;

static void delay(int milliseconds) {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
}

void ledc_demo_init() {
    ESP_LOGI(_LOGGING_TAG, "Initializing leds...");
    _leds = initialize_leds();
    _leds_size = get_leds_size();
}

void ledc_demo_deinit() {}
void run_ledc_demo() {
    const uint16_t switch_interval = 3000;

    ESP_LOGI(_LOGGING_TAG, "Switching led colors to red");
    set_led_color_percent(&_leds[0], 100, 0, 0, 0);
    set_led_color_percent(&_leds[1], 100, 0, 0, 0);
    delay(switch_interval);
    
    ESP_LOGI(_LOGGING_TAG, "Switching led colors to green");
    set_led_color_percent(&_leds[0], 0, 100, 0, 0);
    set_led_color_percent(&_leds[1], 0, 100, 0, 0);
    delay(switch_interval);
    
    ESP_LOGI(_LOGGING_TAG, "Switching led colors to blue");
    set_led_color_percent(&_leds[0], 0, 0, 100, 0);
    set_led_color_percent(&_leds[1], 0, 0, 100, 0);
    delay(switch_interval);
}
