#pragma once
#include "esp_err.h"

int32_t get_leds_size();
struct ledc_rgb_led_t *initialize_leds();