#include "ledc.h"
#include "ledc_init.h"
#include "esp_log.h"

// Macros prefixed with CONFIG_ are set by meunconfig config manager
#define LED_PIN_WEST_RED CONFIG_LED_PIN_WEST_RED
#define LED_PIN_WEST_GREEN CONFIG_LED_PIN_WEST_GREEN
#define LED_PIN_WEST_BLUE CONFIG_LED_PIN_WEST_BLUE

#define LED_PIN_EAST_RED CONFIG_LED_PIN_EAST_RED
#define LED_PIN_EAST_GREEN CONFIG_LED_PIN_EAST_GREEN
#define LED_PIN_EAST_BLUE CONFIG_LED_PIN_EAST_BLUE

#define LED_FADE_INTERVAL CONFIG_PULSE_WIDTH_MODULATION_FADE_INTERVAL
#define CONNECTED_LED_COUNT 2

static const char *_LOGGING_TAG = "ledc_init";
ledc_channel_config_t _rgb_ledc_channels_west[3];
ledc_channel_config_t _rgb_ledc_channels_east[3];
struct ledc_rgb_led_t leds[CONNECTED_LED_COUNT];

const ledc_timer_config_t _low_speed_ledc_timer = {
    .duty_resolution = LEDC_TIMER_13_BIT, // 13bit = 2^13 = 8192 levels within 1 cycle
    .freq_hz = 5000,                      // 5kHz = 1 cycle lasts 1/5000s
    .speed_mode = LEDC_HIGH_SPEED_MODE,    // ESP32-S2 only supports configuring channels in “low speed” mode
    .timer_num = LEDC_TIMER_0,            // PWM timer
    .clk_cfg = LEDC_AUTO_CLK              // Auto select the source clock
};

int32_t get_leds_size()
{
    return CONNECTED_LED_COUNT;
}

static esp_err_t _register_led(
    int id,
    const ledc_timer_config_t _ledc_timer,
    ledc_channel_config_t _ledc_channels_rgb[3],
    int32_t fade_interval)
{
    if (id > get_leds_size())
    {
        ESP_LOGE(_LOGGING_TAG, "Unable to register led with id %i. Id can only range fro 0 to %i.", id, get_leds_size());
        return ESP_FAIL;
    }

    const struct ledc_rgb_led_t led = {
        .name = "southwest",
        .is_initialized = true,
        .is_common_anode = true,
        .fade_milliseconds = fade_interval,
        .red = {
            .name = "red",
            .timer = _ledc_timer,
            .channel = _ledc_channels_rgb[0],
        },
        .green = {
            .name = "green",
            .timer = _ledc_timer,
            .channel = _ledc_channels_rgb[1],
        },
        .blue = {
            .name = "blue",
            .timer = _ledc_timer,
            .channel = _ledc_channels_rgb[2],
        }};

    ESP_LOGI(_LOGGING_TAG, "Registering led%i %s", id, led.name);
    leds[id] = led;
    return ESP_OK;
}

static void _init_rgb_channel(
    ledc_channel_config_t *ledc_config,
    ledc_timer_config_t timer,
    int gpio_pin,
    ledc_channel_t channel,
    bool is_common_anode)
{
    ledc_config->channel = channel;
    ledc_config->duty = is_common_anode ? get_max_duty(&timer) : 0;
    ledc_config->gpio_num = gpio_pin;
    ledc_config->speed_mode = timer.speed_mode;
    ledc_config->hpoint = 0;
    ledc_config->timer_sel = timer.timer_num;
}

static void _setup_rgb_leds()
{
    // Led: West
    struct ledc_rgb_led_t led = leds[0];
    _init_rgb_channel(&_rgb_ledc_channels_west[0], _low_speed_ledc_timer, LED_PIN_WEST_RED, LEDC_CHANNEL_0, led.is_common_anode);
    _init_rgb_channel(&_rgb_ledc_channels_west[1], _low_speed_ledc_timer, LED_PIN_WEST_GREEN, LEDC_CHANNEL_1, led.is_common_anode);
    _init_rgb_channel(&_rgb_ledc_channels_west[2], _low_speed_ledc_timer, LED_PIN_WEST_BLUE, LEDC_CHANNEL_2, led.is_common_anode);
    ESP_ERROR_CHECK(_register_led(0, _low_speed_ledc_timer, _rgb_ledc_channels_west, LED_FADE_INTERVAL));
    
    // Led: East
    led = leds[1];
    _init_rgb_channel(&_rgb_ledc_channels_east[0], _low_speed_ledc_timer, LED_PIN_EAST_RED, LEDC_CHANNEL_3, led.is_common_anode);
    _init_rgb_channel(&_rgb_ledc_channels_east[1], _low_speed_ledc_timer, LED_PIN_EAST_GREEN, LEDC_CHANNEL_4, led.is_common_anode);
    _init_rgb_channel(&_rgb_ledc_channels_east[2], _low_speed_ledc_timer, LED_PIN_EAST_BLUE, LEDC_CHANNEL_5, led.is_common_anode);
    ESP_ERROR_CHECK(_register_led(1, _low_speed_ledc_timer, _rgb_ledc_channels_east, LED_FADE_INTERVAL));
}

struct ledc_rgb_led_t *initialize_leds()
{
    ESP_LOGI(_LOGGING_TAG, "Setting up LED channels...");
    _setup_rgb_leds();

    configure_rgb_led(&leds[1]);
    configure_rgb_led(&leds[0]);

    return leds;
}
