#include "esp_log.h"
#include "wifi.h"
#include "nvs_init.h"
#include "esp_http_server.h"
#include "home_controller.h"
#include "led_controller.h"
#include "lcd_controller.h"
#include "ledc_init.h"
#include "display_colors.h"
#include "display.h"
#include "spiffs_init.h"
#include "ledc_demo.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

// Macros prefixed with CONFIG_ are set by meunconfig config manager
#define WIFI_SSID CONFIG_WIFI_SSID
#define WIFI_PASSWORD CONFIG_WIFI_PASSWORD

static const char *_LOGGING_TAG = "main";
const char *_font_gothic_16x32 = "/spiffs/ILGH32XB.FNT";
esp_vfs_spiffs_conf_t spiffs_config = {
    .base_path = "/spiffs",
    .partition_label = NULL,
    .max_files = 10,
    .format_if_mount_failed = true};

static void delay(int milliseconds) {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
}

/**
 * @brief application entry point
 * @return esp_err_t 
 */
esp_err_t app_main()
{
    // TODO: => Couldn't get this demo to work because a row of other hello-world
    // TODO:    requirements weren't met. This complicates things a lot
    // TODO: Created a small demo for extraction into an "offline" project
    ledc_demo_init();
    run_ledc_demo();
    delay(3000);
    ledc_demo_deinit();

    initialize_non_volatile_storage(); // wifi requirement

    // beware, runs out of scope once main
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASSWORD,
            .threshold.authmode = WIFI_AUTH_WPA2_PSK,
            .pmf_cfg = {
                // Use protected management frames if access point can handle it
                .capable = true,
                .required = false}}};

    ESP_ERROR_CHECK(init_tcpip_networking(&wifi_config));
    if (is_wifi_connection_failed() || !is_wifi_connected())
    {
        ESP_LOGE(_LOGGING_TAG, "Failed to connect to SSID: %s", WIFI_SSID);
        return ESP_FAIL;
    }
    else
    {
        ESP_LOGI(_LOGGING_TAG, "Connected to Ssid: %s", CONFIG_WIFI_SSID);
    }

    ESP_LOGI(_LOGGING_TAG, "Initializing LCD...");
    initialize_spiffs(&spiffs_config); // display requirement
    initialize_display(
        CONFIG_MOSI_GPIO,
        CONFIG_SCLK_GPIO,
        CONFIG_CS_GPIO,
        CONFIG_DC_GPIO,
        CONFIG_RESET_GPIO,
        CONFIG_BL_GPIO,
        CONFIG_WIDTH,
        CONFIG_HEIGHT,
        CONFIG_OFFSETX,
        CONFIG_OFFSETY,
        _font_gothic_16x32);
    fill_screen(get_random_rgb565_color());

    ESP_ERROR_CHECK(start_webserver());

    const struct home_controller_endpoints_t *home_controller_routes = get_home_controller_endpoints();
    const struct led_controller_routes_t *led_controller_routes = get_led_controller_routes();
    const lcd_controller_routes_t *lcd_controller_routes = get_lcd_controller_routes();
    assert(home_controller_routes);
    assert(led_controller_routes);
    assert(lcd_controller_routes);
    ESP_ERROR_CHECK(register_endpoint(home_controller_routes->get_hello));
    ESP_ERROR_CHECK(register_endpoint(home_controller_routes->get_temperature));
    ESP_ERROR_CHECK(register_endpoint(&led_controller_routes->POST_set_led_color));
    ESP_ERROR_CHECK(register_endpoint(&lcd_controller_routes->GET_fill_screen));
    ESP_ERROR_CHECK(register_endpoint(&lcd_controller_routes->POST_draw_text));

    return ESP_OK;
}