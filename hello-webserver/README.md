# Hello Webserver

This project demonstrates the implementation of a webserver through implementation of other hello-world usecases.

## Configuration

This project is a PlatformIO ESP32 ESP-IDF project. For configuration you can use the `menuconfig` tool. You will have to 

## Usage

Wifi credentials are configured using our custom [wifi credentials](./src/Kconfig.projbuild) `menuconfig` section

![vscode screenshot with platformio menuconfig and opened wifi credentials screen](./images/hello_wifi_configuration.jpg)

The generated config file is [gitignored](./.gitignore) so that it can't be committed by accident.

After that, you can `Upload` and `Monitor` to run the application.

![shows a gif of a platformio build and upload run](./images/hello-webserver.gif)
