#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "mcuinfo.h"
#include "number_display.h"

esp_mcuinfo mcuinfo;
NumberDisplay number_display;

// DECLARATIONS
// ------------
// APPLICATION
// -----------
void print_device_info(void)
{
    for (int i = 10; i > 0; i--)
    {
        mcuinfo.print_info();
        // delay(250);
    }
}

void draw_screen(NumberDisplay *display)
{
    auto number = rand() % 100;
    auto filledEllipses = number % 2 == 1;

    display->clearScreen();
    display->displayEllipses(filledEllipses);
    display->displayNumber(number);

    // delay(2000);
}

void sleep_ms(int milliseconds)
{
    vTaskDelay(milliseconds / portTICK_PERIOD_MS);
}

extern "C" void app_main()
{
    print_device_info();
    while (true)
    {
        draw_screen(&number_display);
        sleep_ms(1000);
    }
}