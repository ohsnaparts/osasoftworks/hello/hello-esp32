#include "esp_spi_flash.h"
#include "esp_system.h"
#include <stdio.h>

class esp_mcuinfo {
    private:
        esp_chip_info_t chip_info;
    public:
        void print_info();
};
