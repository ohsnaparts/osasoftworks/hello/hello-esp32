// Variables:
// ----------
// COMPONENT_DIRS, COMPONENTS_DIRS
//    Directories to search for components. Defaults to
//    IDF_PATH/components,
//    PROJECT_DIR/components, and
//    EXTRA_COMPONENT_DIRS.
//    Override this variable if you don’t want to search for components in these places.
//
// EXTRA_COMPONENT_DIRS, EXTRA_COMPONENTS_DIRS
//    Optional list of additional directories to search for components.
//    Paths can be relative to the project directory, or absolute.
//
//
// COMPONENTS
//    A list of component names to build into the project. Defaults to all components found in the
//    COMPONENT_DIRS directories. Use this variable to “trim down” the project for faster build times.
//   Note that any component which “requires” another component via the REQUIRES or PRIV_REQUIRES arguments
//   on component registration will automatically have it added to this list, so the COMPONENTS list can be
//   very short.
//
// Any paths in these variables can be absolute paths, or set relative to the project directory.
//
// Setting Variables:
// ------------------
// Use the cmake set command ie
//    set(VARIABLE "VALUE")
// The set() commands should be placed after the cmake_minimum(...) line but before the include(...) line.
//
// Docs:
// -----
// // https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/build-system.html#optional-project-variables
//
//
// Component registration:
// -----------------------
// idf_component_register([[SRCS src1 src2 ...] | [[SRC_DIRS dir1 dir2 ...] [EXCLUDE_SRCS src1 src2 ...]]
//                        [INCLUDE_DIRS dir1 dir2 ...]
//                        [PRIV_INCLUDE_DIRS dir1 dir2 ...]
//                        [REQUIRES component1 component2 ...]
//                        [PRIV_REQUIRES component1 component2 ...]
//                        [LDFRAGMENTS ldfragment1 ldfragment2 ...]
//                        [REQUIRED_IDF_TARGETS target1 target2 ...]
//                        [EMBED_FILES file1 file2 ...]
//                        [EMBED_TXTFILES file1 file2 ...]
//                        [KCONFIG kconfig]
//                        [KCONFIG_PROJBUILD kconfig_projbuild])
// SRCS:
//    is a list of source files (*.c, *.cpp, *.cc, *.S). These source files will be compiled into the component library
// INCLUDE_DIRS
//    is a list of directories to add to the global include search path for any component which requires this component, and also the main source files
// PRIV_INCLUDE_DIRS
//    directory paths, must be relative to the component directory, which will be added to the include search path for this component’s source files only
// REQUIRES
//    is not actually required, but it is very often required to declare what other components this component will use.