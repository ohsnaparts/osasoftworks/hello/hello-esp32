module.exports = {
  someSidebar: {
    Docusaurus: ['doc1', 'doc2', 'doc3'],
    Features: ['mdx'],
    "mqtt-controlled-led": ['mqtt-controlled-led/index']
  },
};
